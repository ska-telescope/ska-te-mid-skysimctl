#!/bin/bash
echo "Hello, DevContainer!"

# # You know, packages and stuff.
# sudo apt-get update
# sudo apt-get install openssh-client gnupg2 gawk yamllint curl git vim -y

# # Install Helm
# curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

# Install kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm kubectl

# Set KUBECONFIG file in root of project to KUBECONFIG for container
export KUBECONFIG=KUBECONFIG

# Make bash output useful
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\] $ "
