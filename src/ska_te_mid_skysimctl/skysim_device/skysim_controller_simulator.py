"""Simulator for the Sky Sim Controller."""

import logging

from tango.server import command

from ska_te_mid_skysimctl.skysim_device.skysim_controller_base import SkySimCtlDeviceBase


class SkySimCtlDeviceSimulator(SkySimCtlDeviceBase):
    """Simulator for the Sky Sim Controller."""

    def init_device(self):
        """Initialize the device."""
        super().init_device()
        self.__temperature = 0.0

    def read_all_relays(self) -> None:
        """Read all relays."""
        self.logger.info("simulator: read_all_relays")

    def read_temperature(self) -> float:
        """
        Simulate reading temperature from the Raspberry's sensors.

        :return: the temperature
        :rtype: float
        """
        self.logger.info(f"simulator: read_temperature={self.__temperature}")
        return self.__temperature

    @command(doc_in="Reset the device.")
    def reset_device(self):
        """
        Reset the device's state.

        Used in testing to ensure that each test starts from a known state.
        """
        # pylint: disable-next=attribute-defined-outside-init
        self.__temperature = 20.0
        super().init_state()

    @command(dtype_in=float, doc_in="Set the temperature to read.")
    def set_temperature(self, temperature: float):
        """
        Set the max_alarm threshold.

        :param temperature: The new temperature in degrees Celsius.
        :type temperature: float
        """
        self.logger.info(f"simulator: setting temperatures: current={self.__temperature}; new={temperature}")
        # pylint: disable-next=attribute-defined-outside-init
        self.__temperature = temperature


def main():
    """Get the main peanut."""
    logging.basicConfig(level=logging.DEBUG)
    SkySimCtlDeviceSimulator.run_server()
