#! /usr/bin/env python
# mypy: ignore-errors
"""
Proof of concept Tango Device for the MID-ITF's Pi4.

This Device uses the asyncio green mode.
"""
import io
import logging
from enum import Enum
from typing import Dict

from ska_te_mid_skysimctl.skysim_device.skysim_controller_base import SkySimCtlDeviceBase

ADAFRUIT_EXCEPTION = None
ANA_CHAN = None
try:
    import adafruit_ads1x15.ads1015 as ADS
    import board
    import busio
    from adafruit_ads1x15.analog_in import AnalogIn

    i2c = busio.I2C(board.SCL, board.SDA)
    ads = ADS.ADS1015(i2c)
    ANA_CHAN = AnalogIn(ads, ADS.P0)
except ModuleNotFoundError as me:
    ADS = None
    ADAFRUIT_EXCEPTION = me
except AttributeError as ae:
    ADS = None
    ADAFRUIT_EXCEPTION = ae
except ValueError as ve:
    ADS = None
    ADAFRUIT_EXCEPTION = ve

PI_PLATE_EXCEPTION = None
try:
    import piplates.RELAYplate as RELAY

    PI_PLATE_ADDR = 0
except ModuleNotFoundError as me:
    PI_PLATE_ADDR = -1
    PI_PLATE_EXCEPTION = me
except RuntimeError as re:
    PI_PLATE_ADDR = -1
    PI_PLATE_EXCEPTION = re


class SkySimCtlDevice(SkySimCtlDeviceBase):
    """The Raspberry Pi device."""

    RELAY_ADDRESS = PI_PLATE_ADDR

    def switch_relay(self, relay_pin: str, onoff: bool) -> None:
        """
        Switch relay on or off.

        :param relay_pin: relay pin number
        :param onoff: state of relay
        """
        super().switch_relay(relay_pin, onoff)
        if self.RELAY_ADDRESS >= 0:
            if onoff is True:
                RELAY.relayON(self.RELAY_ADDRESS, self.RELAY_TARGETS[relay_pin])
            else:
                RELAY.relayOFF(self.RELAY_ADDRESS, self.RELAY_TARGETS[relay_pin])
        else:
            self.logger.warning("Skip switching relay")

    def switch_relays(self, relay_pins: Dict[str, bool]) -> None:
        """
        Switches multiple relays on/off.

        :param relay_pins: a dict of relay pin numbers -> desired relay state.
        """
        for pin, state in relay_pins.items():
            self.switch_relay(pin, state)

    def read_all_relays(self) -> None:
        """Read all relays."""
        if self.RELAY_ADDRESS >= 0:
            # pylint: disable-next=attribute-defined-outside-init
            self.all_states = RELAY.relaySTATE(self.RELAY_ADDRESS)
            for relay, pin in self.RELAY_TARGETS.items():
                self.relays[relay] = (self.all_states >> (pin - 1)) & 1 == 1
            self.logger.debug("Read all relays: %s", self.relays)
        else:
            self.logger.warning("Skip reading relays")

    def read_temperature(self) -> float:
        """
        Read the temperature from the Raspberry Pi hardware sensor.

        :return: The temperature in degrees Celsius.
        :rtype: float
        """
        if ANA_CHAN is None:
            # everything is fine, I promise
            return 27.0
        return round((((ANA_CHAN.voltage * 1000) - 500) / 10) * (-1), 2)


class PiModel(Enum):
    """Represents the current Raspberry Pi model."""

    PI0 = "Pi Zero"
    PI4 = "Pi 4"
    UNKNOWN = "UNKNOWN"
    SIMULATOR = "SIMULATOR"


def raspberrypi_model() -> PiModel:
    """
    Determine the Raspberry Pi model .

    See https://raspberrypi.stackexchange.com/a/118473.

    :returns: The Raspberry Pi Model as a PiModel enum.
    """
    pi0 = "Raspberry Pi Zero"
    pi4 = "Raspberry Pi 4"
    try:
        with io.open("/sys/firmware/devicetree/base/model", "r", encoding="utf8") as model_file:
            model_line = model_file.read()
            if pi0 in model_line:
                return PiModel.PI0
            if pi4 in model_line:
                return PiModel.PI4
            return PiModel.UNKNOWN
    except FileNotFoundError:
        return PiModel.SIMULATOR


def __raise_import_exceptions():
    model = raspberrypi_model()
    if model == PiModel.UNKNOWN:
        raise ValueError("Unsupported Raspberry Pi Model")
    if model == PiModel.SIMULATOR:
        return
    if PI_PLATE_EXCEPTION is not None:
        raise PI_PLATE_EXCEPTION
    # Temperature sensor is not used on Pi0
    if model == PiModel.PI0:
        return
    if ADAFRUIT_EXCEPTION is not None:
        # raise ADAFRUIT_EXCEPTION
        # temp workaround for broken temperature sensor
        logging.info("ignoring ADAFRUIT_EXCEPTION due to broken temperature sensor: %s", ADAFRUIT_EXCEPTION)


def main():
    """Get the main peanut."""
    logging.basicConfig(level=logging.DEBUG)
    __raise_import_exceptions()
    logging.info("Start with piplate %s and DHT sensor %s", PI_PLATE_ADDR, ANA_CHAN)
    SkySimCtlDevice.run_server()


if __name__ == "__main__":
    main()
