# mypy: ignore-errors
"""
Proof of concept Tango Device for the MID-ITF's Pi4.

This Device uses the asyncio green mode.
"""
import logging
from enum import IntEnum
from typing import Dict

# pylint: disable-next=no-name-in-module
from PyTango import MultiAttrProp
from tango import AttrQuality, DevDouble, DevState, DevUChar, DevVoid, DispLevel
from tango.server import Device, attribute, command


def on_off(state: bool) -> str:
    """
    Convert a bool to an ON/OFF state string.

    :param state: the value to convert
    :returns: ON if True, OFF otherwise
    """
    if state:
        return "ON"
    return "OFF"


class BandState(IntEnum):
    """BandState is the current state of all the Bands in the relay."""

    # This should never be used: it is only here because tango
    # requires enums to start at 0.
    INVALID = 0
    BAND1 = 1  # Band_1 is ON
    BAND2 = 2  # Band_2 is ON
    BAND3 = 3  # Band_3 is ON

    def __str__(self) -> str:
        """
        Get the string representation of the BandState.

        :returns: the name of the enum value in title case.
        """
        return self.name.title()


class SkySimCtlDeviceBase(Device):
    """The Raspberry Pi device."""

    RELAY_TARGETS = {
        "Correlated_Noise_Source": 1,
        "H_Channel": 2,
        "V_Channel": 3,
        "Uncorrelated_Noise_Sources": 4,
        "Band1": 5,
        "Band2": 6,
        "Band3": 7,
    }
    RELAY_TARGETS_REVERSED = dict(map(reversed, RELAY_TARGETS.items()))
    relays = None

    def init_device(self):
        """Initialize the device."""
        super().init_device()
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        self.set_change_event("temperature", True, True)
        self.set_change_event("humidity", True, True)
        self.set_archive_event("temperature", True, True)
        self.set_archive_event("humidity", True, True)

        self.init_state()
        self.logger.info("Device initialised")

    def init_state(self):
        """Initialise the device's state."""
        # pylint: disable=attribute-defined-outside-init
        self.relays = {
            "Correlated_Noise_Source": False,
            "H_Channel": False,
            "V_Channel": False,
            "Uncorrelated_Noise_Sources": False,
            "Band1": False,
            # Default to Band2 switched ON.
            # It is invalid to have all 3 Bands switched OFF.
            "Band2": True,
            "Band3": False,
        }
        self.all_states = 0
        self.__last_alarm_state = AttrQuality.ATTR_VALID
        self.__stored_signal_values = None
        self.read_all_relays()
        self.set_state(DevState.ON)

    def switch_relay(self, relay_pin: str, onoff: bool) -> None:
        """
        Switch relay on or off.

        :param relay_pin: relay pin number
        :param onoff: state of relay
        """
        self.relays[relay_pin] = onoff
        self.logger.info("Switch pin %s to %s", relay_pin, self.relays[relay_pin])

    def switch_relays(self, relay_pins: Dict[str, bool]) -> None:
        """
        Switches multiple relays on/off.

        :param relay_pins: a dict of relay pin numbers -> desired relay state.
        """
        for pin, state in relay_pins.items():
            self.switch_relay(pin, state)

    def read_all_relays(self) -> None:
        """
        Read all relays.

        :raises RuntimeError: method not implemented
        """
        raise RuntimeError("not implemented")

    def read_temperature(self) -> float:
        """
        Read temperature from the Raspberry's sensors.

        :raises RuntimeError: method not implemented
        """
        raise RuntimeError("not implemented")

    def __set_thresholds(self, max_warning: float = -1.0, max_alarm: float = -1.0):
        """
        Set the temperature thresholds.

        :param max_warning: The max_warning threshold to set. Not set if the value is negative.
        :type max_warning: float
        :param max_alarm: The max_alarm threshold to set. Not set if the value is negative.
        :type max_alarm: float
        """
        multi_prop = MultiAttrProp()
        multi_attr = self.get_device_attr()
        attr_temperature = multi_attr.get_attr_by_name("temperature")
        multi_prop = attr_temperature.get_properties()
        if max_warning >= 0:
            multi_prop.max_warning = str(max_warning)
        if max_alarm >= 0:
            multi_prop.max_alarm = str(max_alarm)
        attr_temperature.set_properties(multi_prop)
        self.logger.info(f"max_warning={multi_prop.max_warning}, max_alarm={multi_prop.max_alarm}")

    def update_temperature_alarm(self, temperature: float) -> None:
        """
        Monitor temperatue warning and alarm.

        :param temperature: the new temperature: this is a parameter this method is called from the attribute.
        :type temperature: float
        """
        multi_attr = self.get_device_attr()
        attr_temperature = multi_attr.get_attr_by_name("temperature")
        multi_prop = attr_temperature.get_properties()
        temp_warning = float(multi_prop.max_warning)
        temp_alarm = float(multi_prop.max_alarm)
        current_state = AttrQuality.ATTR_VALID
        if temperature >= temp_alarm:
            current_state = AttrQuality.ATTR_ALARM
        elif temperature >= temp_warning:
            current_state = AttrQuality.ATTR_WARNING

        self.logger.info(
            f"current_state {current_state} previous_state={self.__last_alarm_state}, "
            f"temperature={temperature}, max_warning={temp_warning}, max_alarm={temp_alarm}"
        )

        # Read alarm status and switch noise generators
        if current_state == AttrQuality.ATTR_WARNING:
            # Enforced cool down phase A2
            if self.__last_alarm_state == AttrQuality.ATTR_ALARM:
                self.logger.debug("cooldown stage A2: not enabling noise sources")
        elif current_state == AttrQuality.ATTR_ALARM:
            if self.__last_alarm_state != AttrQuality.ATTR_ALARM:
                self.logger.debug(f"disabling noise sources: last_alarm_state=={self.__last_alarm_state}")
                self.__disable_noise_sources()
            else:
                self.logger.debug(f"not disabling noise sources: last_alarm_state=={self.__last_alarm_state}")
        # Alarm states is attr_valid, reset all states to normal
        elif current_state == AttrQuality.ATTR_VALID:
            if self.__last_alarm_state != AttrQuality.ATTR_VALID:
                self.logger.debug(f"enabling noise sources: last_alarm_state=={self.__last_alarm_state}")
                self.__enable_noise_sources()
            else:
                self.logger.debug(f"not enabling noise sources: last_alarm_state=={self.__last_alarm_state}")
        else:
            # Factors such as ATTR_INVALID, unhandled
            self.logger.info(f"unhandled state {current_state}")
            return

        # pylint: disable-next=attribute-defined-outside-init
        self.__last_alarm_state = current_state

    def __disable_noise_sources(self) -> None:
        # noise sources already disabled: nothing to do
        if self.__stored_signal_values is not None:
            return
        # Disable noise sources when in alarm
        self.logger.info("ALARM: disabling noise sources")
        # pylint: disable-next=attribute-defined-outside-init
        self.__stored_signal_values = {
            "Correlated_Noise_Source": self.relays["Correlated_Noise_Source"],
            "Uncorrelated_Noise_Sources": self.relays["Uncorrelated_Noise_Sources"],
        }
        self.__set_corr_relays(False)
        self.__set_uncorr_relays(False)

    def __enable_noise_sources(self) -> None:
        # noise sources already updated: nothing to do
        if self.__stored_signal_values is None:
            return
        self.logger.info("cooldown complete: enabling noise sources")
        self.__set_corr_relays(self.__stored_signal_values["Correlated_Noise_Source"])
        self.__set_uncorr_relays(self.__stored_signal_values["Uncorrelated_Noise_Sources"])
        # pylint: disable-next=attribute-defined-outside-init
        self.__stored_signal_values = None

    def __set_corr_relays(self, val) -> None:
        self.logger.info("Setting Correlated_Noise_Source=%s", val)
        self.relays["Correlated_Noise_Source"] = val
        self.switch_relay("Correlated_Noise_Source", val)

    def __set_uncorr_relays(self, val) -> None:
        self.logger.info("setting Uncorrelated_Noise_Sources=%s", val)
        self.relays["Uncorrelated_Noise_Sources"] = val
        self.switch_relay("Uncorrelated_Noise_Sources", val)

    @attribute(
        polling_period=5000,
        archive_rel_change=1.0,
        rel_change=1.0,
        label="temperature",
        dtype=DevDouble,
        display_level=DispLevel.EXPERT,
        unit="°C",
        format="8.4f",
        min_value=0,
        max_value=100.0,
        doc="temperature of the SkySimCtrl",
        min_warning=10.0,
        max_warning=50.0,
        min_alarm=-2.0,
        max_alarm=60.0,
    )
    def temperature(self) -> DevDouble:
        """
        Read current temperature.

        :returns: current temperature in Celcius
        """
        temp = self.read_temperature()
        self.update_temperature_alarm(temp)
        return temp

    @attribute(polling_period=5000, archive_rel_change=1.0, rel_change=1.0)
    def humidity(self) -> DevDouble:
        """
        Read current humidity.

        :returns: current humidity percentage
        """
        return 0.0

    @attribute(dtype=DevUChar)
    # pylint: disable-next=invalid-name
    def All_Relay_States(self) -> DevUChar:
        """
        Read all relay states.

        :returns: all the states
        """
        self.read_all_relays()
        return self.all_states

    @attribute(dtype=bool)
    # pylint: disable-next=invalid-name
    def Correlated_Noise_Source(self) -> bool:
        """
        Read state of Correlated Noise Source.

        :returns: relay state
        """
        self.read_all_relays()
        return self.relays["Correlated_Noise_Source"]

    @Correlated_Noise_Source.write
    # pylint: disable-next=invalid-name
    def Correlated_Noise_Source(self, value: bool) -> DevVoid:
        """
        Set state of Correlated Noise Source.

        :param value: new state of relay
        """
        self.logger.info("Set Correlated_Noise_Source to %s", value)
        self.relays["Correlated_Noise_Source"] = value
        self.switch_relay("Correlated_Noise_Source", value)

    @attribute(dtype=bool)
    # pylint: disable-next=invalid-name
    def H_Channel(self) -> bool:
        """
        Read state of H_Channel.

        :returns: relay state
        """
        self.read_all_relays()
        return self.relays["H_Channel"]

    @H_Channel.write
    # pylint: disable-next=invalid-name
    def H_Channel(self, value: bool) -> DevVoid:
        """
        Set state of H_Channel.

        :param value: new state of relay
        """
        self.logger.info("Set H_Channel to %s", value)
        self.relays["H_Channel"] = value
        self.switch_relay("H_Channel", value)

    @attribute(dtype=bool)
    # pylint: disable-next=invalid-name
    def V_Channel(self) -> bool:
        """
        Read state of V_Channel.

        :returns: relay state
        """
        self.read_all_relays()
        return self.relays["V_Channel"]

    @V_Channel.write
    # pylint: disable-next=invalid-name
    def V_Channel(self, value: bool) -> DevVoid:
        """
        Set state of V_Channel.

        :param value: new state of relay
        """
        self.logger.info("Set V_Channel to %s", value)
        self.relays["V_Channel"] = value
        self.switch_relay("V_Channel", value)

    @attribute(dtype=bool)
    # pylint: disable-next=invalid-name
    def Uncorrelated_Noise_Sources(self) -> bool:
        """
        Read state of Uncorrelated_Noise_Sources.

        :returns: relay state
        """
        self.read_all_relays()
        return self.relays["Uncorrelated_Noise_Sources"]

    @Uncorrelated_Noise_Sources.write
    # pylint: disable-next=invalid-name
    def Uncorrelated_Noise_Sources(self, value: bool) -> DevVoid:
        """
        Set state of Uncorrelated_Noise_Sources.

        :param value: new state of relay
        """
        self.logger.info("Set Uncorrelated_Noise_Sources to %s", value)
        self.relays["Uncorrelated_Noise_Sources"] = value
        self.switch_relay("Uncorrelated_Noise_Sources", value)

    @attribute(dtype=BandState)
    # pylint: disable-next=invalid-name
    def Band(self) -> BandState:
        """
        Retrieve the current Active Band.

        If more than one Band is active, an Exception is raised.

        :returns: active Band in the relay
        """
        self.read_all_relays()
        band1 = self.relays["Band1"]
        band2 = self.relays["Band2"]
        band3 = self.relays["Band3"]
        active_band = BandState.BAND2
        if band1 and not (band2 or band3):
            active_band = BandState.BAND1
        elif band2 and not (band1 or band3):
            active_band = BandState.BAND2
        elif band3 and not (band1 or band2):
            active_band = BandState.BAND3
        else:
            # Switch to the default band if no Band is set
            self.logger.info("No Band was set: switching to default - Band 2")
            self.switch_relays({"Band2": True})
            active_band = BandState.BAND2
        self.logger.info("active band: %s", active_band)
        return active_band

    @Band.write
    # pylint: disable-next=invalid-name
    def Band(self, value: BandState) -> DevVoid:
        """
        Set the current active Band.

        If the value is not supported, nothing is set and an error is logged.

        :param value: new active Band in the relay

        :raises ValueError: if parametrized with a value other than
                            BandState.BAND1, BandState.BAND2 or BandState.BAND3.
        """
        band_state = BandState(value)
        if band_state not in [BandState.BAND1, BandState.BAND2, BandState.BAND3]:
            raise ValueError(f"Invalid BandState {value} specified")

        changes = {
            "Band1": False,
            "Band2": False,
            "Band3": False,
        }
        changes[str(band_state)] = True
        self.switch_relays(changes)
        self.logger.info("%s switched ON", band_state)

    @command(dtype_in=float, doc_in="Set the maximum warning threshold in °C.")
    def temperature_max_warning_threshold(self, max_warning: float):
        """
        Set the max_warning threshold.

        :param max_warning: The new max_warning threshold in degrees Celsius.
        :type max_warning: float
        """
        self.__set_thresholds(max_warning=max_warning)

    @command(dtype_in=float, doc_in="Set the maximum alarm threshold in °C.")
    def temperature_max_alarm_threshold(self, max_alarm: float):
        """
        Set the max_alarm threshold.

        :param max_alarm: The new max_alarm threshold in degrees Celsius.
        :type max_alarm: float
        """
        self.__set_thresholds(max_alarm=max_alarm)
