#!/usr/bin/env python
# mypy: ignore-errors
"""Quick and dirty web server for testing Raspberry Pi connectivity."""
import logging
from pathlib import Path

import tango
import uvicorn
from fastapi import FastAPI, HTTPException, Request
from fastapi.templating import Jinja2Templates
from pydantic import BaseSettings
from starlette.responses import Response


# pylint: disable-next=too-few-public-methods
class SkySimWebSettings(BaseSettings):
    """SkySimWebSettings defines configuration for the webserver."""

    tango_host: str
    device_instance: str
    templates_dir: str = str(Path(__file__).resolve().parent / "templates")
    skysimweb_host: str
    skysimweb_port: int = 80


settings = SkySimWebSettings()
app = FastAPI(title="SkySimCTl Web View", openapi_url="/openapi.json")


@app.api_route("/", status_code=200, methods=["GET", "HEAD"])
def root(request: Request) -> Response:
    """
    Display the current state of the skysimctl.

    :param request: the HTTP request.
    :raises HTTPException: if the connection to the tangodb fails.
    :return: A response containing the template and parameters which will be rendered.
    """
    skysimctl_tango_device = f"mid-itf/skysimctl/{settings.device_instance}"
    try:
        device_proxy = tango.DeviceProxy(skysimctl_tango_device)
        return __templates().TemplateResponse(
            "index.html",
            {"request": request, "device_proxy": device_proxy},
        )

    except tango.DevFailed as device_failed_exception:
        logging.exception("Failed to connect to skysimctl tango device")
        raise HTTPException(
            status_code=500,
            detail=f"Failed to connect to skysimctl: TANGO_HOST={settings.tango_host},"
            f"SKYSIMCTL_TANGO_DEVICE={skysimctl_tango_device}",
        ) from device_failed_exception


@app.exception_handler(500)
def custom_500_handler(request: Request, exception: HTTPException):
    """
    Display 500 errors.

    :param request: the HTTP request.
    :param exception: an exception containing error details.
    :return: A response containing the template and parameters which will be rendered.
    """
    return __templates().TemplateResponse("error.html", {"request": request, "exception": exception})


def __templates():
    return Jinja2Templates(directory=settings.templates_dir)


if __name__ == "__main__":
    uvicorn.run(app, host=settings.skysimweb_host, port=settings.skysimweb_port)
