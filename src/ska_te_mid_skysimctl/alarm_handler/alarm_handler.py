"""Alarm handler for the MID-ITF's Pi4."""

import logging
import os
import threading
from enum import Enum
from queue import Queue
from time import sleep

import tango
from slack_sdk.webhook import WebhookClient
from tango import AttrQuality, DeviceProxy

logging.basicConfig(level=logging.DEBUG)

__all__ = ["SkySimAlarmHandler", "main"]


class AlarmState(Enum):
    """AlarmState is the state of temperature alarm."""

    OK = 0
    WARNING = 1
    ALARM = 2

    def in_alarm(self) -> bool:
        """
        Determine whether the AlarmState results in the device being in 'alarm'.

        :return: Whether the AlarmState is in 'alarm'.
        :rtype: bool
        """
        return self in [AlarmState.ALARM, AlarmState.WARNING]


def get_alarm_state(attr_quality: AttrQuality) -> AlarmState:
    """
    Create an AlarmState from a tango.AttrQuality value.

    :param attr_quality: The tango AttrQuality value.
    :type attr_quality: AttrQuality
    :return: The corresponding AlarmState.
    :rtype: AlarmState
    """
    if attr_quality == AttrQuality.ATTR_WARNING:
        return AlarmState.WARNING
    if attr_quality == AttrQuality.ATTR_ALARM:
        return AlarmState.ALARM
    return AlarmState.OK


class SlackMsg:
    """Slack message object."""

    # pylint: disable=too-few-public-methods
    def __init__(self, temperature: float, alarm_state: AlarmState, prev_alarm_state: AlarmState, event_time: float):
        """
        Initialise attributes of the slack message object.

        :param temperature: current temperature of the skysim
        :type temperature: float
        :param alarm_state: current state of temperature alarm
        :type alarm_state: AlarmState
        :param prev_alarm_state: previous state of temperature alarm
        :type prev_alarm_state: AlarmState
        :param event_time: time at which this event fired
        :type event_time: float
        """
        self.temperature = temperature
        self.alarm_state = alarm_state
        self.prev_alarm_state = prev_alarm_state
        self.event_time = event_time


class SkySimAlarmHandler:
    """Alarm handler for the MID-ITF's Pi4."""

    # pylint: disable=too-many-instance-attributes
    def __init__(
        self, tango_device_proxy: DeviceProxy, webhook_url: str, skysimctl_commands_url: str, sleep_interval: int = 2
    ):
        """
        Initialise the attributes of the AlarmHandler.

        :param tango_device_proxy: The skysimctl tango device.
        :type tango_device_proxy: DeviceProxy
        :param webhook_url: The webhook to send the Slack message on.
        :type webhook_url: str
        :param skysimctl_commands_url: The URL where temperature thresholds can be changed.
        :type skysimctl_commands_url: str
        :param sleep_interval: How long the handler should sleep at every interval in its main loop.
        :type sleep_interval: int
        """
        self.logger = logging.getLogger(__name__)
        self.__prev_alarm_state = AlarmState.OK
        self.__last_message_time = 0.0
        self.__stopped = False
        self.__stop_lock = threading.Lock()
        self.__device_proxy = tango_device_proxy
        self.__webhook_url = webhook_url
        self.__skysimctl_commands_url = skysimctl_commands_url
        self.__sleep_interval = sleep_interval
        self.__http_queue: Queue = Queue(100)
        self.logger.debug("device initialised")
        self.event_subscription()

    def event_subscription(self):
        """Subscribe to events: specifically temperature change events."""
        self.__device_proxy.subscribe_event(
            "temperature",
            tango.EventType.CHANGE_EVENT,
            self.handle_temperature_event,
            stateless=False,
        )
        self.logger.info("subscribed to events")

    def handle_temperature_event(self, evt):
        """
        Handle a temperature change event.

        :param evt: The temperature change event
        :type evt: tango.EventData
        """
        self.logger.debug(f"Received event: {evt}")
        if evt.err:
            error = evt.errors[0]
            self.logger.error(f"ERROR EVENT: {error.reason}: {error.desc}\n\nDETAILS: {evt}")
            return

        event_time = evt.attr_value.time.totime()
        temp = float(evt.attr_value.value)
        alarm_state = get_alarm_state(evt.attr_value.quality)
        slack_msg = SlackMsg(temp, alarm_state, self.__prev_alarm_state, event_time)
        self.add_to_queue(slack_msg)
        self.__prev_alarm_state = alarm_state

    def add_to_queue(self, slack_msg: SlackMsg):
        """
        Append new slack message to queue.

        :param slack_msg: The slack message object
        """
        self.__http_queue.put(slack_msg)

    def process_send_msg_queue(self) -> None:
        """Consume and send each messages in the message queue."""
        while not self.stopped():
            if not self.__http_queue.empty():
                msg: SlackMsg = self.__http_queue.get()
                self.send_message(msg)
            sleep(2)

    # pylint: disable-next=too-many-locals
    def send_message(self, slack_msg: SlackMsg) -> None:
        """
        Send a slack message to channel on temperature threshold.

        :param slack_msg: slack message object
        """
        temperature = slack_msg.temperature
        alarm_state = slack_msg.alarm_state
        prev_alarm_state = slack_msg.prev_alarm_state
        event_time = slack_msg.event_time
        if not alarm_state.in_alarm() and not prev_alarm_state.in_alarm():
            return

        time_since_last_message = event_time - self.__last_message_time
        if alarm_state.in_alarm() and alarm_state == prev_alarm_state and time_since_last_message < 600:
            self.logger.debug(f"Only {time_since_last_message} seconds since last alert: not sending message")
            return

        tmp_config = self.__device_proxy.get_attribute_config("temperature")
        max_warning = float(tmp_config.alarms.max_warning)
        max_alarm = float(tmp_config.alarms.max_alarm)
        min_warning = float(tmp_config.alarms.min_warning)
        min_alarm = float(tmp_config.alarms.min_alarm)
        self.logger.debug(
            (
                f"Sending Slack alert: current_alarm_state - {alarm_state}, "
                f"prev_alarm_state: {prev_alarm_state}, time_since_last_message: {time_since_last_message}s, "
                f"temperature - {temperature}, min_warning - {min_warning}, max_warning - {max_warning}, "
                f"min_alarm - {min_alarm}, max_alarm - {min_alarm}"
            )
        )

        msg_type = alarm_state.name
        lines = []
        if alarm_state == AlarmState.OK:
            lines = [
                f"*SkySimCtl {prev_alarm_state.name.title()} cleared!*",
                f"Temperature is at {round(temperature, 3)} °C",
            ]
        else:
            lines = [
                f"*{msg_type}: SkySimCtl temperature is at {round(temperature, 3)} °C*",
            ]

        lines.extend(
            [
                f"\tAlarm Thresholds - Min: {min_alarm} °C, Max: {max_alarm} °C",
                f"\tWarning Thresholds - Min: {min_warning} °C, Max: {max_warning} °C",
                f"*<{self.__skysimctl_commands_url}|Update Thresholds>*.",
            ]
        )
        msg = "\n".join(lines)
        webhook = WebhookClient(self.__webhook_url)
        response = webhook.send(
            text="fallback",
            blocks=[
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": msg,
                    },
                }
            ],
        )

        if response.status_code != 200:
            self.logger.error(
                f"Failed to send to slack: {response.status_code}",
            )
        else:
            self.logger.info("Alert sent to slack")
            self.__last_message_time = event_time

    def status(self) -> str:
        """
        Get the current running status of the alarm handler.

        :return: The alarm handler status.
        :rtype: str
        """
        if self.__stopped:
            return "stopped"
        return "running"

    def run(self):
        """Run continuously so that we receive events."""
        sleep_total = 0
        msg_processing_thread = threading.Thread(target=self.process_send_msg_queue, daemon=True)
        msg_processing_thread.start()

        while not self.stopped():
            sleep(self.__sleep_interval)
            sleep_total += self.__sleep_interval
            self.logger.info("Alarm Handler has been running for %d seconds", sleep_total)

        msg_processing_thread.join()

    def stopped(self) -> bool:
        """
        Return whether the alarm handler has stopped or not.

        :return: Whether the alarm handler has stopped or not.
        :rtype: bool
        """
        result = False
        with self.__stop_lock:
            result = self.__stopped
        return result

    def stop(self):
        """Stop the alarm handler."""
        with self.__stop_lock:
            self.__stopped = True


def main():
    """Run the AlarmHandler."""
    webhook_url = os.environ["SKYSIM_ALARM_WEBHOOK"]
    tango_device = os.environ["SKYSIM_DEVICE_PI4"]
    tango_device_proxy = DeviceProxy(tango_device)
    cluster_domain = os.environ["CLUSTER_DOMAIN"]
    kube_namespace = os.environ["KUBE_NAMESPACE"]
    skysimctl_commands_url = f"https://k8s.{cluster_domain}/{kube_namespace}/taranta/devices/{tango_device}/commands"
    alarm_handler = SkySimAlarmHandler(tango_device_proxy, webhook_url, skysimctl_commands_url)
    alarm_handler.run()


if __name__ == "__main__":
    main()
