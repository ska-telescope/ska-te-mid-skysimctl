"""Webserver which handles posts to Slack webhook during testing."""

import json
import logging
from http.server import BaseHTTPRequestHandler, HTTPServer
from sys import argv


class SlackTestWebServer:
    """Webserver which handles posts to Slack webhook during testing."""

    def __init__(self, ip_address: str = "0.0.0.0", port: int = 8090):
        """
        Create the webserver.

        :param ip_address: The IP address on which to serve, defaults to "0.0.0.0"
        :type ip_address: str
        :param port: The port on which to serve, defaults to 8080
        :type port: int
        """
        logging.basicConfig(level=logging.DEBUG)
        server_address = (ip_address, port)
        self.__server = HTTPServer(server_address, handler_factory(self))
        self.__alarms = 0
        self.__warnings = 0
        self.__cleared = 0
        self.__stopped = True

    def run(self):
        """Run the webserver."""
        self.__stopped = False
        logging.info("Starting SlackTestWebServer...")
        try:
            self.__server.serve_forever()
        except KeyboardInterrupt:
            pass
        self.__server.server_close()
        logging.info("Stopping SlackTestWebServer...")

    def stop(self):
        """Signal that the server should stop."""
        self.__server.shutdown()
        self.__stopped = True

    def status(self) -> str:
        """
        Get the current running status of the webserver.

        :return: The webserver status.
        :rtype: str
        """
        if self.__stopped:
            return "stopped"
        return "running"

    def get_counts(self) -> dict:
        """
        Retrieve the counts.

        :return: The alarm, warning and cleared counts.
        :rtype: dict
        """
        body = {"alarms": self.__alarms, "warnings": self.__warnings, "cleared": self.__cleared}
        return body

    def clear_counts(self):
        """Clear the counts."""
        self.__alarms = 0
        self.__warnings = 0
        self.__cleared = 0

    def inc_alarms(self):
        """Increment the alarm count."""
        self.__alarms += 1
        logging.info(
            f"alarms: {self.__alarms}; warnings: {self.__warnings}; cleared: {self.__cleared}",
        )

    def inc_warnings(self):
        """Increment the warning count."""
        self.__warnings += 1
        logging.info(
            f"alarms: {self.__alarms}; warnings: {self.__warnings}; cleared: {self.__cleared}",
        )

    def inc_cleared(self):
        """Increment the cleared count."""
        self.__cleared += 1
        logging.info(
            f"alarms: {self.__alarms}; warnings: {self.__warnings}; cleared: {self.__cleared}",
        )


class SlackTestHandler(BaseHTTPRequestHandler):
    """Webserver which handles posts to Slack webhook during testing."""

    def __init__(self, webserver: SlackTestWebServer, *args, **kwargs):
        """
        Initialise the handler.

        :param webserver: The webserver used for callbacks.
        :type webserver: SlackTestWebServer
        :param args: args
        :param kwargs: kwargs
        """
        self.webserver = webserver
        super().__init__(*args, **kwargs)

    def _set_response(self, content_type: str = "text/html"):
        self.send_response(200)
        self.send_header("Content-type", content_type)
        self.end_headers()

    # pylint: disable-next=invalid-name
    def do_GET(self):
        """Handle a GET request."""
        logging.debug(f"GET request,\nPath: {self.path}\nHeaders:\n{self.headers}")
        body = self.webserver.get_counts()
        body_b = json.dumps(body).encode("utf-8")
        self._set_response(content_type="application/json;charset=utf-8")
        self.wfile.write(body_b)

    # pylint: disable-next=invalid-name
    def do_POST(self):
        """Handle a POST request."""
        content_length = int(self.headers["Content-Length"])
        post_data = json.loads(self.rfile.read(content_length).decode("utf-8"))
        logging.info(
            f"POST request,\nPath: {self.path}\nHeaders:\n{self.headers}\n\nBody:\n{post_data}\n",
        )
        text = post_data["blocks"][0]["text"]["text"]
        if "ALARM: SkySimCtl temperature" in text:
            self.webserver.inc_alarms()
        elif "WARNING: SkySimCtl temperature" in text:
            self.webserver.inc_warnings()
        else:
            self.webserver.inc_cleared()
        self._set_response()
        self.wfile.write(f"POST request for {self.path}".encode("utf-8"))


def handler_factory(webserver: SlackTestWebServer):
    """
    Create a SlackTestHandler. Required so that we can give it params.

    :param webserver: The SlackTestWebServer for callbacks.
    :type webserver: SlackTestWebServer
    :returns: SlackTestHandler
    """

    def create_handler(*args, **kwargs):
        return SlackTestHandler(webserver, *args, **kwargs)

    return create_handler


def main():
    """Run the webserver."""
    if len(argv) == 2:
        port = int(argv[1])
    else:
        port = 8090
    webserver = SlackTestWebServer(port=port)
    webserver.run()


if __name__ == "__main__":
    main()
