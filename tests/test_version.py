"""This module tests the ska_te_mid_skysimctl version."""

import ska_te_mid_skysimctl


def test_version() -> None:
    """Test that the ska_te_mid_skysimctl version is as expected."""
    assert ska_te_mid_skysimctl.__version__ == "0.2.2"
