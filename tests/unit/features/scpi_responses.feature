Feature: SCPI unit tests

    Scenario Outline: Test if SkysimController SCPI responds correctly

        Given the SkySimController SCPI device is listening
        When I send the SCPI command
        # When I send the SCPI command <scpi_command>
        Then the device responds to <scpi_command> with <expected_response>

        Examples:

            | scpi_command | expected_response                   |
            | HELP         | Send SYST:HELP for help             |
            # | *IDN?          | Skysim controller 0.0.1             |
            # | *CLS           | ok                                  |
            # | INIT           | ok                                  |
            # | *TST?          | All systems go                      |
            # | SYST:VERS      | 0.0.1                               |
            # | SYST:CTYP      | Skysim controller 0.0.1             |
            | SYST:DATE    | Current time - fix this in the test |
# | SYST:ERR?      | 0, No error                         |
# | OUTP1:START    | ok                                  |
# | OUTP1?         | 1                                   |
# | OUTP1:STOP     | ok                                  |
# | OUTP2:START    | ok                                  |
# | OUTP2?         | 1                                   |
# | OUTP2:STOP     | ok                                  |
# | OUTP3:START    | ok                                  |
# | OUTP3?         | 1                                   |
# | OUTP3:STOP     | ok                                  |
# | OUTP4:START    | ok                                  |
# | OUTP4?         | 1                                   |
# | OUTP4:STOP     | ok                                  |
# | OUTP5:START    | ok                                  |
# | OUTP5?         | 1                                   |
# | OUTP5:STOP     | ok                                  |
# | OUTP6:START    | ok                                  |
# | OUTP6?         | 1                                   |
# | OUTP6:STOP     | ok                                  |
# | OUTP7:START    | ok                                  |
# | OUTP7?         | 1                                   |
# | OUTP7:STOP     | ok                                  |
# | OUTP8:START    | ok                                  |
# | OUTP8?         | 1                                   |
# | OUTP8:STOP     | ok                                  |
# | *STB?          | 00                                  |
# | test this      | huh?                                |
# | SYST:ERR:ALL?  | 1, Invalid command 'TEST THIS'      |
# | SYST:ERR:COUN? | 1                                   |
