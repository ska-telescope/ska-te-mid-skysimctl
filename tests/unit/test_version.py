"""This module tests the package version."""

from pytest_bdd import given, parsers, scenario, then

from ska_te_mid_skysimctl import __version__ as version


# @pytest.mark.skip
@scenario("features/version.feature", "Test version of skysimctl package")
def test_version_of_skysimctl_package():
    """Test version of skysimctl package."""


@given("an imported ska_te_mid_skysimctl package")
def imported_package():
    """Fails if there is no imported ska_te_mid_skysimctl package."""
    print(version)


@then(parsers.parse("its version is {test_version}"))
def check_version(test_version):
    """
    Package version is 0.2.2.

    :param test_version: the version of the imported package - see Jira issue.
    """
    assert version == test_version
