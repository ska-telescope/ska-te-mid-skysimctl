#!/usr/bin/env python
# mypy: ignore-errors
"""SCPI unit tests feature tests."""

import datetime
import logging

import pytest
from pytest_bdd import given, parsers, scenario, then, when


@pytest.mark.xfail
@scenario(
    "features/scpi_responses.feature",
    "Test if SkysimController SCPI responds correctly",
)
def test_responses():
    """Test if SkysimController SCPI device responds correctly."""


@given("the SkySimController SCPI device is listening")
def setup_connection():
    """Given the SkySimController SCPI device is listening."""


@when(
    parsers.parse("I send the SCPI command {scpi_command:d}"),
    target_fixture="received_response",
)
def send_cmd(scpi_command):
    """When I send the SCPI command <scpi_command>.

    :param scpi_command: SCPI command sent
    :return: The target Fixture.
    """
    logging.debug(" SCPI Command %s sent", scpi_command)

    # TODO: replace all, send cmd_sent to the SCPI device and
    # return target fixture value
    if scpi_command == "DATE":
        return datetime.datetime.now().strftime("%Y-%m-%d")
    return "Not Implemented Yet"


@then("the device responds to <scpi_command> with <expected_response>")
def check_response(received_response, scpi_command, expected_response):
    """
    Check if the device responds with <scpi_response>.

    :param received_response: Fixture with the received response
    :param scpi_command: SCPI command sent
    :param expected_response: _description_
    """
    if scpi_command == "DATE":
        assert received_response == datetime.datetime.now().strftime("%Y-%m-%d")
    else:
        # TODO: use scpi_response instead of "Not Implemented Yet"
        assert received_response == "Not Implemented Yet", (
            "Epic fail, received " + received_response + f"but expected {expected_response}"
        )
