#!/usr/bin/env python
# mypy: ignore-errors
"""This module contains test harness elements common to all unit tests."""
import logging

import pytest


@pytest.fixture()
def logger() -> logging.Logger:
    """
    Fixture that returns a default logger.

    The logger will be set to DEBUG level, as befits testing.

    :return: a logger
    """
    debug_logger = logging.getLogger()
    debug_logger.setLevel(logging.DEBUG)
    return debug_logger
