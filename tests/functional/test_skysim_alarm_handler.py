"""Test that noise sources are switched ON/OFF by changes in temperature."""

import logging
from time import sleep

import tango
from pytest_bdd import given, scenario, then, when

from ska_te_mid_skysimctl.alarm_handler.alarm_handler import SkySimAlarmHandler
from ska_te_mid_skysimctl.alarm_handler.slack_test_webserver import SlackTestWebServer
from tests.functional.conftest import set_temperature


@scenario(
    "features/skysim_alarm_handler.feature",
    "SkySimAlarmHandler generates Slack alarm alerts when temperature exceeds the alarm threshold",
)
def test_skysimalarmhandler_generates_slack_alerts_when_temperature_exceeds_alarm_threshold():
    """Test SkySimAlarmHandler generates Slack alarm alerts when temperature exceeds the alarm threshold."""
    logging.info("SkySimAlarmHandler generates Slack alarm alerts when temperature exceeds the alarm threshold")


@given("the temperature is below the warning threshold")
def given_temperature_below_warning_threshold(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Set the temperature below the warning threshold.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    set_temperature(skysim_controller_device, 40)
    logging.info(f"temperature changed to {skysim_controller_device.temperature}")


@given("the SlackTestWebServer is running")
def given_the_slacktestwebserver_is_running(
    slack_test_webserver: SlackTestWebServer,
) -> None:
    """
    Start the SlackTestWebServer.

    :param slack_test_webserver: the slack test webserver
    :type slack_test_webserver: SlackTestWebServer
    """
    assert slack_test_webserver.status() == "running"
    logging.info("the SlackTestWebServer is running")


@given("the SkySimAlarmHandler is running")
def given_the_skysimalarmhandler_is_running(
    skysim_alarm_handler: SkySimAlarmHandler,
) -> None:
    """
    Start the SkySimAlarmHandler.

    :param skysim_alarm_handler: the skysim alarm handler
    :type skysim_alarm_handler: SkySimAlarmHandler
    """
    assert skysim_alarm_handler.status() == "running"
    logging.info("the SkySimAlarmHandler is running")


@then("wait for the temperature attribute poll period")
def wait_for_the_temperature_attribute_poll_period() -> None:
    """Wait for the temperature attribute poll period."""
    sleep(6)
    logging.info("waited for the temperature attribute poll period")


@then("clear SlackTestWebServer alerts")
def clear_slacktestwebserver_alerts(slack_test_webserver: SlackTestWebServer):
    """
    Clear SlackTestWebServer alerts.

    :param slack_test_webserver: the slack test webserver
    :type slack_test_webserver: SlackTestWebServer
    """ """"""
    slack_test_webserver.clear_counts()
    logging.info("cleared SlackTestWebServer alerts")


@then("the SlackTestWebServer receives an Alarm alert")
def the_slacktestwebserver_receives_an_alarm_alert(
    slack_test_webserver: SlackTestWebServer,
):
    """
    Assert that the SlackTestWebServer receives an Alarm alert.

    :param slack_test_webserver: The slack test webserver.
    :type slack_test_webserver: SlackTestWebServer
    """
    counts = slack_test_webserver.get_counts()
    assert counts["alarms"] == 1
    assert counts["warnings"] == 0
    assert counts["cleared"] == 0
    logging.info("the SlackTestWebServer receives an Alarm alert")


@scenario(
    "features/skysim_alarm_handler.feature",
    "SkySimAlarmHandler generates Slack warning alerts when temperature exceeds the warning threshold",
)
def test_skysimalarmhandler_generates_slack_alerts_when_temperature_exceeds_warning_threshold():
    """Test SkySimAlarmHandler generates Slack warning alerts when temperature exceeds the warning threshold."""
    logging.info("SkySimAlarmHandler generates Slack warning alerts when temperature exceeds the warning threshold")


@when("the temperature goes above the warning threshold")
def temp_above_warning_threshold(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Set the temperature above the warning threshold.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    set_temperature(skysim_controller_device, 55)
    logging.info(f"temperature changed to above warning threshold: temperature={skysim_controller_device.temperature}")


@then("the SlackTestWebServer receives a Warning alert")
def the_slacktestwebserver_receives_a_warning_alert(
    slack_test_webserver: SlackTestWebServer,
):
    """
    Verify that the SlackTestWebServer receives a Warning alert.

    :param slack_test_webserver: the SlackTestWebServer
    :type slack_test_webserver: SlackTestWebServer
    """
    counts = slack_test_webserver.get_counts()
    assert counts["alarms"] == 0
    assert counts["warnings"] == 1
    assert counts["cleared"] == 0
    logging.info("the SlackTestWebServer receives a Warning alert")


@scenario(
    "features/skysim_alarm_handler.feature",
    "SkySimAlarmHandler generates Slack Alarm cleared alerts when temperature goes below the warning threshold",
)
def test_skysimalarmhandler_generates_slack_alarm_cleared_alerts_when_temperature_goes_below_warning_threshold():
    """Test that SkySimAlarmHandler clears alerts when temperature goes below the warning threshold."""
    logging.info(
        "SkySimAlarmHandler generates Slack Alarm cleared alerts when temperature goes below the warning threshold"
    )


@then("the SlackTestWebServer receives a Warning cleared alert")
def the_slacktestwebserver_receives_a_warning_cleared_alert(
    slack_test_webserver: SlackTestWebServer,
):
    """
    Assert that the SlackTestWebServer receives a Warning cleared alert.

    :param slack_test_webserver: Slack test webserver
    :type slack_test_webserver: SlackTestWebServer
    """
    counts = slack_test_webserver.get_counts()
    assert counts["alarms"] == 0
    assert counts["warnings"] == 1
    assert counts["cleared"] == 1
    logging.info("the SlackTestWebServer receives a Warning cleared alert")


@given("the SkySimController is initialised")
def the_skysimcontroller_is_initialised(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Initialise SkySimController.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info(f"GIVEN: the SkySimController is initialised: state={skysim_controller_device.State()}")
    skysim_controller_device.reset_device()
