"""Test that noise sources are switched ON/OFF by changes in temperature."""

import logging

import tango
from pytest_bdd import given, scenario, then

from tests.functional.conftest import set_temperature, wait_for_condition, wait_for_noise_sources


@scenario(
    "features/skysim_controller_temperature.feature",
    "SkySimController switches off noise sources when temperature breaches threshold",
)
def test_skysimcontroller_switches_off_noise_sources_when_temperature_breaches_threshold():
    """Test SkysimController switches off noise sources when temperature breaches threshold."""
    logging.info("TEST: SkySimController switches off noise sources when temperature breaches threshold")


@given("the temperature is below the alarm threshold")
def given_temperature_below_threshold(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Set the temperature below the alarm threshold.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("GIVEN: the temperature is below the alarm threshold")
    set_temperature(skysim_controller_device, 40)
    logging.info(f"temperature changed to {skysim_controller_device.temperature}")


@given("the noise sources are switched ON")
def noise_sources_on(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Switch all noise sources ON.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("GIVEN: the noise sources are switched ON")
    skysim_controller_device.Uncorrelated_Noise_Sources = True
    skysim_controller_device.Correlated_Noise_Source = True
    wait_for_noise_sources(skysim_controller_device, True)


@then("it switches off the noise sources")
def it_switches_off_the_noise_sources(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Check that the noise sources are switched OFF.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("THEN: it switches off the noise sources")
    wait_for_noise_sources(skysim_controller_device, False)
    assert not skysim_controller_device.Correlated_Noise_Source
    assert not skysim_controller_device.Uncorrelated_Noise_Sources


@scenario(
    "features/skysim_controller_temperature.feature",
    "SkySimController switches noise sources back on when temperature goes under warning threshold",
)
def test_skysimcontroller_switches_on_noise_sources_when_temperature_goes_under_threshold():
    """Test SkySimController switches noise sources back on when temperature goes under warning threshold."""
    logging.info("TEST: SkySimController switches noise sources back on when temperature goes under warning threshold")


@then("the noise sources are switched OFF")
def then_noise_sources_off(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Check that the noise sources are switched OFF.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("THEN: the noise sources are switched OFF")
    wait_for_noise_sources(skysim_controller_device, False)


@then("the noise sources are switched ON")
def noise_sources_switched_on(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Wait a bit and make sure that the noise sources are switched back ON.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("THEN: the noise sources are switched ON")
    wait_for_noise_sources(skysim_controller_device, True)


@then("It provides the temperature in degrees celsius as an attribute")
def provide_temperature(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Check that skysimctl provides temperature.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("THEN: It provides the temperature in degrees celsius as an attribute")
    logging.info(f"temperature is: {skysim_controller_device.temperature}°C")


@scenario(
    "features/skysim_controller_temperature.feature",
    "SkySimController restores noise sources to previous state when temperature goes under warning threshold",
)
def test_skysimcontroller_restores_noise_sources_previous_state_when_temperature_goes_under_threshold():
    """Test SkySimController restores noise sources to previous state when temperature goes under warning threshold."""
    logging.info(
        "TEST: SkySimController restores noise sources to previous state when temperature goes under warning threshold."
    )


@given("the correlated noise source is switched ON")
def given_correlated_noise_source_on(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Switch the correlated noise source ON.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    :raises TimeoutError: if the condition is not met.
    """
    logging.info("GIVEN: the correlated noise source is switched ON")
    skysim_controller_device.Correlated_Noise_Source = True

    def check_correlated() -> bool:
        return skysim_controller_device.Correlated_Noise_Source

    succeeded, sleep_time = wait_for_condition(check_correlated)
    if not succeeded:
        raise TimeoutError(
            f"Correlated Noise source did not get turned ON after {sleep_time} seconds; "
            f"Correlated_Noise_Source={skysim_controller_device.Correlated_Noise_Source}"
        )


@given("the uncorrelated noise sources are switched OFF")
def given_uncorrelated_noise_sources_off(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Switch the uncorrelated noise sources OFF.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    :raises TimeoutError: if the condition is not met.
    """
    logging.info("GIVEN: the uncorrelated noise sources are switched OFF")
    skysim_controller_device.Uncorrelated_Noise_Sources = False

    def check_uncorrelated() -> bool:
        return not skysim_controller_device.Uncorrelated_Noise_Sources

    succeeded, sleep_time = wait_for_condition(check_uncorrelated)
    if not succeeded:
        raise TimeoutError(
            f"Uncorrelated Noise sources did not get turned OFF after {sleep_time} seconds; "
            f"Uncorrelated_Noise_Sources={skysim_controller_device.Uncorrelated_Noise_Sources}"
        )


@then("the correlated noise source is switched ON")
def then_correlated_noise_source_on(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Check that the correlated noise source is ON.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    :raises TimeoutError: if the condition is not met
    """
    logging.info("THEN: the correlated noise source is switched ON")

    def check_correlated() -> bool:
        return skysim_controller_device.Correlated_Noise_Source

    succeeded, sleep_time = wait_for_condition(check_correlated)
    if not succeeded:
        raise TimeoutError(
            f"Correlated Noise source did not get turned ON after {sleep_time} seconds; "
            f"Correlated_Noise_Source={skysim_controller_device.Correlated_Noise_Source}"
        )


@then("the uncorrelated noise sources are switched OFF")
def then_uncorrelated_noise_sources_off(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Check that the uncorrelated noise sources are OFF.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    :raises TimeoutError: if the condition is not met.
    """
    logging.info("THEN: the uncorrelated noise sources are switched OFF")

    def check_uncorrelated() -> bool:
        return not skysim_controller_device.Uncorrelated_Noise_Sources

    succeeded, sleep_time = wait_for_condition(check_uncorrelated)
    if not succeeded:
        raise TimeoutError(
            f"Uncorrelated Noise sources did not get turned OFF after {sleep_time} seconds; "
            f"Uncorrelated_Noise_Sources={skysim_controller_device.Uncorrelated_Noise_Sources}"
        )


@given("the SkySimController is initialised")
def the_skysimcontroller_is_initialised(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Initialise SkySimController.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info(f"GIVEN: the SkySimController is initialised: state={skysim_controller_device.State()}")
    skysim_controller_device.reset_device()
