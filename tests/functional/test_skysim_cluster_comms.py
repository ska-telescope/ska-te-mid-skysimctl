"""
Sky Simulator components can be controlled remotely.

Using Tango device server running on a Raspberry Pi feature tests.
"""

import os

import pytest
import requests
from pytest_bdd import given, scenario, then, when

pytest.k8s_running = False
pytest.response_200 = ""


@pytest.mark.xfail(reason="Temporarily xfail: this runs against the cluster. Need to conditionally run it.")
@scenario(
    "features/skysim_cluster_comms.feature",
    "Test if the SkySimCtl host can be reached from inside the kubernetes cluster.",
)
def test_test_if_the_skysimctl_host_can_be_reached_from_inside_the_kubernetes_cluster():
    """Test if the SkySimCtl host can be reached from inside the kubernetes cluster.."""


@given("a kubernetes cluster")
def kubernetes_cluster_is_running():
    """Check if kubernetes cluster is running."""
    if os.environ.get("CI_RUNNER_TAGS") == '["ska-k8srunner-za-itf"]':
        pytest.k8s_running = True
    assert pytest.k8s_running


@when("I send a HTTP GET request to a test server running on the SkySimCtl device")
def cluster_sends_http_to_skysim_ctrl():
    """Send HTTP GET request to a test server running on the SkySimCtl device."""
    if pytest.k8s_running:
        result = requests.head(os.environ.get("SKYSIMCTL_ENDPOINT"), timeout=30)
        pytest.response_200 = result.status_code


@then("a 200 response code must be received.")
def skysim_ctrl_response_ok():
    """Receive a 200 HTTP response from skymim-ctrl."""
    assert 200 == pytest.response_200
