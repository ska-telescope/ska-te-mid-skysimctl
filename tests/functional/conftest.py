#!/usr/bin/env python
# mypy: ignore-errors
"""This module contains functional test harness and pytest configuration."""
# isort: skip_file
from threading import Thread
import logging
from typing import (
    Generator,
    Optional,
    cast,
    Any,
)
from time import sleep
import pytest
from pytest_bdd import given, when
import tango
from ska_control_model import LoggingLevel
from ska_tango_testing.context import (
    TangoContextProtocol,
    ThreadedTestTangoContextManager,
)
from ska_te_mid_skysimctl.skysim_device.skysim_controller_simulator import (
    SkySimCtlDeviceSimulator,
)
from ska_te_mid_skysimctl.alarm_handler.alarm_handler import SkySimAlarmHandler
from ska_te_mid_skysimctl.alarm_handler.slack_test_webserver import SlackTestWebServer


def pytest_bdd_apply_tag(
    tag: str,
    function: pytest.Function,
) -> Optional[bool]:
    """
    Override how pytest_bdd maps feature file tags to pytest marks.

    :param tag: the tag in the feature file.
    :param function: the python function that pytest_bdd associates the
        tag with

    :return: whether this hook has handled the tag.
    """
    if tag.startswith("XTP-"):
        marker = pytest.mark.xray(tag)
        marker(function)
        return True

    # Fall back to pytest-bdd's default handler
    return None


@pytest.fixture(name="tango_device_name", scope="session")
def tango_device_name_fixture() -> Generator[str, None, None]:
    """
    Yield the tango device name triplet identifying the device to test.

    :yield: the tango device name triplet
    :rtype: Generator[str, None, None]
    """
    yield "test-itf/skysimctl/1"


@pytest.fixture(name="tango_context", scope="session")
def tango_context_fixture(tango_device_name: str) -> Generator[TangoContextProtocol, None, None]:
    """
    Yield a Tango context containing the devices under test.

    :param tango_device_name: the device name triplet.
    :yields: a Tango context containing the devices under test
    :rtype: ThreadedTestTangoContextManager
    """
    context_manager = ThreadedTestTangoContextManager()
    cast(ThreadedTestTangoContextManager, context_manager).add_device(
        tango_device_name,
        SkySimCtlDeviceSimulator,
        UpdateRate=1.0,
        LoggingLevelDefault=int(LoggingLevel.DEBUG),
    )
    with context_manager as context:
        yield context


@pytest.fixture(name="skysim_controller_device", scope="session")
def skysim_controller_device_fixture(
    tango_context: TangoContextProtocol,
    tango_device_name: str,
) -> Generator[tango.DeviceProxy, None, None]:
    """
    Return a proxy to the skysim controller device.

    :param tango_context: the context in which the device is running.
    :param tango_device_name: the device name triplet.
    :yields: a proxy to the sky sim controller device.
    :rtype: tango.DeviceProxy
    """
    yield tango_context.get_device(tango_device_name)


@pytest.fixture(name="slack_webserver_port", scope="session")
def slack_webserver_port_fixture() -> Generator[int, None, None]:
    """
    Retrieve the Slack port used for testing.

    :yields: the Slack port used for testing.
    :rtype: int
    """
    yield 12345


@pytest.fixture(name="slack_webserver_url", scope="session")
def slack_webserver_url_fixture(slack_webserver_port: int) -> Generator[str, None, None]:
    """
    Retrieve the Slack URL used for testing.

    :param slack_webserver_port: port on which the Slack server is listening.
    :type slack_webserver_port: int
    :yields: the Slack URL used for testing.
    :rtype: str
    """
    yield f"http://localhost:{slack_webserver_port}"


@pytest.fixture(name="slack_test_webserver", scope="session")
def slack_test_webserver_fixture(
    slack_webserver_port: int,
) -> Generator[SlackTestWebServer, None, None]:
    """
    Create a SlackTestWebServer fixture.

    :param slack_webserver_port: The port to listen on.
    :type slack_webserver_port: int
    :yield: A SlackTestWebServer
    :rtype: SlackTestWebServer
    """
    webserver = SlackTestWebServer(ip_address="", port=slack_webserver_port)
    webserver_thread = Thread(target=webserver.run, daemon=True)
    webserver_thread.start()
    yield webserver
    webserver.stop()
    webserver_thread.join()


@pytest.fixture(name="skysim_alarm_handler", scope="session")
def skysim_alarm_handler_fixture(
    skysim_controller_device: tango.DeviceProxy,
    slack_webserver_url: str,
) -> Generator[SkySimAlarmHandler, None, None]:
    """
    Create a SkySimAlarmHandler fixture.

    :param skysim_controller_device: DeviceProxy to skysimctl.
    :type skysim_controller_device: tango.DeviceProxy
    :param slack_webserver_url: URL to post Slack alerts to.
    :type slack_webserver_url: str
    :yield: A skysim alarm handler
    :rtype: SkySimAlarmHandler
    """
    alarm_handler = SkySimAlarmHandler(skysim_controller_device, slack_webserver_url, 1)
    alarm_handler_thread = Thread(target=alarm_handler.run, daemon=True)
    alarm_handler_thread.start()
    yield alarm_handler
    alarm_handler.stop()
    alarm_handler_thread.join()


def set_temperature(skysim_controller_device: tango.DeviceProxy, temperature: float):
    """
    Set the temperature via the simulator interface and wait for it to propogate through to the attribute.

    :param skysim_controller_device: the skysimctl DeviceProxy.
    :type skysim_controller_device: tango.DeviceProxy
    :param temperature: the new temperature value.
    :type temperature: float
    :raises TimeoutError: when the temperature does not propagate through to the DeviceProxy attribute.
    """
    skysim_controller_device.set_temperature(temperature)

    def check_temperature() -> bool:
        current_temperature = int(skysim_controller_device.temperature)
        expected = int(temperature)
        logging.info(f"checking if current temperature ({current_temperature}°C)  == expected ({expected}°C)")
        return current_temperature == expected

    succeeded, sleep_time = wait_for_condition(check_temperature)
    if not succeeded:
        raise TimeoutError(
            f"Temperature did not get set to {temperature} after {sleep_time} seconds; "
            f"current temperature: {skysim_controller_device.temperature}"
        )


def wait_for_noise_sources(
    skysim_controller_device: tango.DeviceProxy,
    value: bool,
):
    """
    Wait for noise sources to return the desired value.

    :param skysim_controller_device: the skysimctl DeviceProxy.
    :type skysim_controller_device: tango.DeviceProxy
    :param value: the desired value.
    :type value: bool
    :raises TimeoutError: when the expected value does not propagate through to the DeviceProxy attribute.
    """

    def check_noise_sources() -> bool:
        logging.info(
            f"checking if correlated ({skysim_controller_device.Correlated_Noise_Source}) "
            f"and uncorrelated ({skysim_controller_device.Uncorrelated_Noise_Sources}) "
            f"noise sources == {value}"
        )
        logging.info(f"current temperature: {skysim_controller_device.temperature}")
        return (
            skysim_controller_device.Correlated_Noise_Source == value
            and skysim_controller_device.Uncorrelated_Noise_Sources == value
        )

    succeeded, sleep_time = wait_for_condition(check_noise_sources)
    if not succeeded:
        raise TimeoutError(
            f"Noise sources did not get set to {value} after {sleep_time} seconds; "
            f"Correlated_Noise_Source={skysim_controller_device.Correlated_Noise_Source}, "
            f"Uncorrelated_Noise_Sources={skysim_controller_device.Uncorrelated_Noise_Sources}"
        )


@when("the temperature goes above the alarm threshold")
def temp_above_alarm_threshold(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Set the temperature above the alarm threshold.

    :param skysim_controller_device: the Sky Sim Controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("WHEN: the temperature goes above the alarm threshold")
    set_temperature(skysim_controller_device, 70)


@when("the temperature goes below the warning threshold")
def temp_below_threshold(
    skysim_controller_device: tango.DeviceProxy,
) -> None:
    """
    Set the temperature below the warning threshold.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info("WHEN: the temperature goes below the warning threshold")
    set_temperature(skysim_controller_device, 30)


@given("the SkySimController is initialised")
def the_skysimcontroller_is_initialised(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Initialise SkySimController.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info(f"GIVEN: the SkySimController is initialised: state={skysim_controller_device.State()}")
    skysim_controller_device.reset_device()


@given("the SkySimController is online")
def the_skysimcontroller_is_online(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Turn SkySimController on.

    :param skysim_controller_device: The skysim controller.
    :type skysim_controller_device: tango.DeviceProxy
    """
    logging.info(f"GIVEN: the SkySimController is online: state={skysim_controller_device.State()}")


def wait_for_condition(
    condition: Any,
    retries: int = 8,
    backoff_seconds: float = 0.1,
) -> (bool, float):
    """
    Wait for a condition to be met.

    :param condition: The condition to be met, should be a function which returns a bool.
    :type condition: Any
    :param retries: Number of retries, defaults to 8
    :type retries: int
    :param backoff_seconds: Initial sleep period, defaults to 0.1
    :type backoff_seconds: float
    :return: A tuple with the boolean indicating success and the float the amount of time spent waiting.
    :rtype: (bool, float)
    """
    tries = 0
    total_sleep_time = 0
    while tries < retries:
        if condition():
            return True, total_sleep_time
        sleep_time = backoff_seconds * (2**tries)
        sleep(sleep_time)
        tries += 1
        total_sleep_time += sleep_time
        logging.info(f"waited for {total_sleep_time} seconds")
    return False, total_sleep_time
