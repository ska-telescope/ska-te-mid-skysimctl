@SP-2854
Feature: Skysim components can be controlled remotely using Tango device server that communicates with Raspberry Pi using SCPI commands
	#Currently up to seven simple hardware devices can be switched ON/OFF remotely using GPIO control from a Raspberry Pi (RPi). This is already working, but access to the RPi is needed, which involves SSH connection and additional screens open. This leaves room for improvement.
	#
	#We can use the SCPI device simulators currently available in the SKA Test Equipment repository and instead of simulating a device we can potentially call simple GPIO commands as response to incoming SCPI commands. If the SCPI commands are simple enough, the Tango Device Servers are already available with which we can then control SCPI devices from Taranta / Jupyter Notebooks, and implementing these Device Servers should enable us to monitor and control the Skysim components in similar ways as other Test Equipment.
	#
	#Implementing the SkySim Controller in a web server running on the Raspberry Pi that can be connected to using a TCP socket (which is how the current Device Servers connect to the Device Simulators), is then all that is needed to control the hardware from Tango UIs.
	#
	#
	#
	#
	#

	#Assert whether a RPi SCPI device simulator can receive SCPI commands and respond as expected
	#
	#So that we can develop the SkySim controller Tango device in the same way as the other Test Equipment Tango devices.
	#
	#This test should pass when executed against the real HW as well as (first) the SkySimControllerSimulator (a Simulator that runs alongside the Tango Device Servers in the same k8s cluster).
	@AT-319 @AT-318 @AT-317
	Scenario Outline: Test SkysimController can switch ON signal sources
		Given the SkysimController is online for <signal_source_name>
		And the SkysimController is initialised for <signal_source_name>
		When I switch on the <signal_source_name>
		Then the <signal_source_name> must be ON

		Examples:

			| signal_source_name         |
			| Correlated_Noise_Source    |
			| H_Channel                  |
			| V_Channel                  |
			| Uncorrelated_Noise_Sources |
			| Band1                      |
			| Band2                      |
			| Band3                      |

	#Test if, when asked its name, the Skysim Controller responds as expected.
	@AT-320 @AT-318 @AT-317
	Scenario: Test SCPI device returns identity
		Given the SkysimController is online
		And the SkysimController is initialised
		When I ask its identity
		Then it responds "SkysimController"

	Scenario: Test SkysimController is initialised
		Given all signal sources OFF
		Then it responds "initialised"

