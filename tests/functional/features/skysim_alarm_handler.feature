@SP-2854
Feature: Skysim components can be controlled remotely using Tango device server that communicates with Raspberry Pi using SCPI commands
    @AT-1369
    Scenario: SkySimAlarmHandler generates Slack alarm alerts when temperature exceeds the alarm threshold
        Given the SkySimController is initialised
        And the temperature is below the warning threshold
        And the SlackTestWebServer is running
        And the SkySimAlarmHandler is running
        Then wait for the temperature attribute poll period
        And clear SlackTestWebServer alerts
        When the temperature goes above the alarm threshold
        Then wait for the temperature attribute poll period
        And the SlackTestWebServer receives an Alarm alert

    @AT-1035
    Scenario: SkySimAlarmHandler generates Slack warning alerts when temperature exceeds the warning threshold
        Given the SkySimController is initialised
        And the temperature is below the warning threshold
        And the SlackTestWebServer is running
        And the SkySimAlarmHandler is running
        Then wait for the temperature attribute poll period
        And clear SlackTestWebServer alerts
        When the temperature goes above the warning threshold
        Then wait for the temperature attribute poll period
        And the SlackTestWebServer receives a Warning alert

    @AT-1370
    Scenario: SkySimAlarmHandler generates Slack Alarm cleared alerts when temperature goes below the warning threshold
        Given the SkySimController is initialised
        And the temperature is below the warning threshold
        And the SkySimAlarmHandler is running
        And the SlackTestWebServer is running
        Then wait for the temperature attribute poll period
        And clear SlackTestWebServer alerts
        When the temperature goes above the warning threshold
        Then wait for the temperature attribute poll period
        When the temperature goes below the warning threshold
        Then wait for the temperature attribute poll period
        And the SlackTestWebServer receives a Warning cleared alert
