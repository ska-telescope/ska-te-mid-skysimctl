@SP-2854
Feature: Sky Simulator components can be controlled remotely using Tango device server running on a Raspberry Pi
	#Currently up to seven simple hardware devices can be switched ON/OFF remotely using GPIO control from a Raspberry Pi (RPi).
	#
	#We use the piPlates library in a sample script, with success.
	#
	#We need a Tango Device Server running on the RPi that provides the simple Tango Attributes, in the same fashion that the other Test Equipment, so that these signals can be remotely controlled with the DeviceProxy created in a Jupyter Notebook or in a Taranta dashboard.
	#
	#The Programmable Attenuator has a simple Telnet interface, that could potentially also be communicated with from the same Raspberry Pi web server. It has two commands: Set Attenuation, and Read Attenuation. These should be added to the same Sky Simulator Controller Tango Device.
	#
	#The development of this took a wrong turn during PI17 and if needed, continuing with this work will be discarded for PI18, but if at all possible we'd take the development of the Tango device as a "mini project" during Tango Training, which will be given to the Atlas Team, during PI18.
	#
	#Understanding what the GPIO pins should control and setting up the repo has already progressed significantly in PI17, and so not too much work is left for PI18.

	#This test assumes a host in the 10.165.3.0/24 range. 
	@AT-520 @AT-318 @AT-317
	Scenario: Test if the SkySimCtl host can be reached from inside the kubernetes cluster.
		Given a kubernetes cluster
		When I send a HTTP GET request to a test server running on the SkySimCtl device
		Then a 200 response code must be received.