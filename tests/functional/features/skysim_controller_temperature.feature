@SP-2854
Feature: Skysim components can be controlled remotely using Tango device server that communicates with Raspberry Pi using SCPI commands
    @AT-1168
    Scenario: SkySimController switches off noise sources when temperature breaches threshold
        Given the SkySimController is initialised
        Given the temperature is below the alarm threshold
        And the noise sources are switched ON
        When the temperature goes above the alarm threshold
        Then it switches off the noise sources

    @AT-1367
    Scenario: SkySimController switches noise sources back on when temperature goes under warning threshold
        Given the SkySimController is initialised
        Given the noise sources are switched ON
        When the temperature goes above the alarm threshold
        Then the noise sources are switched OFF
        When the temperature goes below the warning threshold
        Then the noise sources are switched ON

    @AT-1026
    Scenario: SkySimController returns its temperature
        Given the SkySimController is initialised
        Then It provides the temperature in degrees celsius as an attribute

    @AT-1481
    Scenario: SkySimController restores noise sources to previous state when temperature goes under warning threshold
        Given the SkySimController is initialised
        Given the correlated noise source is switched ON
        And the uncorrelated noise sources are switched OFF
        When the temperature goes above the alarm threshold
        Then the noise sources are switched OFF
        When the temperature goes below the warning threshold
        Then the correlated noise source is switched ON
        And the uncorrelated noise sources are switched OFF
