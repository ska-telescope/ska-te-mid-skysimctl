"""Test core Sky Sim Controller functionality."""

import logging

import pytest
import tango
from pytest_bdd import given, parsers, scenario, then, when

from ska_te_mid_skysimctl.skysim_device.skysim_controller_base import BandState


@pytest.mark.xfail(reason="Temporarily xfail as I get errors, caused maybe by offline skysim.")
@scenario("features/skysim_controller.feature", "Test SCPI device returns identity")
def test_scpi_device_returns_identity(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Test SCPI device returns identity.

    :param skysim_controller_device: tango Device Proxy
    """
    identity = getattr(skysim_controller_device, "identity")
    logging.info("Identity is %s", identity)


@scenario(
    "features/skysim_controller.feature",
    "Test SkysimController can switch ON signal sources",
)
def test_skysimcontroller_can_switch_on_signal_sources():
    """Test SkysimController can switch ON signal sources."""
    logging.info("Check signal sources")


@pytest.mark.skip
@scenario("features/skysim_controller.feature", "Test SCPI device returns identity")
def test_skysimcontroller_identity(
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Test SkysimController identity.

    :param skysim_controller_device: tango Device Proxy
    """
    identity = getattr(skysim_controller_device, "identity")
    logging.info("Identity is %s", identity)


@scenario(
    "features/skysim_controller.feature",
    "Test SkysimController is initialised",
)
def test_skysimcontroller_is_initialised():
    """Test SkysimController is initialised."""
    logging.info("Test SkysimController is initialised")


@given("all signal sources OFF")
def all_signal_sources_off():
    """All signal sources OFF."""
    logging.info("all signal sources OFF")


@given(parsers.parse("the SkySimController is online for {signal_source_name}"))
def the_skysimcontroller_is_online_for_signal_source_name(signal_source_name: str) -> None:
    """
    Turn SkySimController on.

    :param signal_source_name: Name of the signal source
    """
    logging.info("Set as on for %s", signal_source_name)


@given(parsers.parse("the SkySimController is initialised for {signal_source_name}"))
def the_skysimcontroller_is_initialised_for_signal_source_name(
    signal_source_name: str,
    skysim_controller_device: tango.DeviceProxy,
):
    """
    Initialise SkySimController.

    :param signal_source_name: Name of the signal source
    :param skysim_controller_device: the device proxy for the skysimctl.
    """
    logging.info("Set as initialised for %s", signal_source_name)
    skysim_controller_device.reset_device()


@when("I ask its identity")
def i_ask_its_identity():
    """I ask its identity."""
    logging.info("ask identity")


@when("I ask its status")
def i_ask_its_status():
    """I ask its status."""
    logging.info("ask status")


@when(parsers.parse("I switch on the {signal_source_name}"))
def i_switch_on_the_signal_source_name(
    skysim_controller_device: tango.DeviceProxy,
    signal_source_name: str,
) -> None:
    """Switch on the <signal_source_name>.

    :param skysim_controller_device: DeviceProxy to SkySim device
    :param signal_source_name: Name of the signal source
    """
    logging.info("switch on the %s", signal_source_name)
    if signal_source_name.startswith("Band"):
        set_band = BandState[signal_source_name.upper()]
        setattr(skysim_controller_device, "Band", set_band)
    else:
        setattr(skysim_controller_device, signal_source_name, True)


@then('it responds "SKYSIMCTL"')
def it_responds_skysimctl():
    """Respond with SKYSIMCTL."""
    logging.info("it responds 'SKYSIMCTL'")


@then('it responds "SkysimController"')
def it_responds_skysimcontroller():
    """Respond with SkysimController."""
    logging.info("it responds 'SkysimController'")


@then('it responds "initialised"')
def it_responds_initialised():
    """Respond with initialised."""
    logging.info("it responds 'initialised'")


@then('it responds "online"')
def it_responds_online():
    """Respond with online."""
    logging.info("it responds 'online'")


@then(parsers.parse("the {signal_source_name} must be ON"))
def the_signal_source_name_must_be_on(
    skysim_controller_device: tango.DeviceProxy,
    signal_source_name: str,
):
    """Verify that <signal_source_name> is ON.

    :param skysim_controller_device: DeviceProxy to SkySim device
    :param signal_source_name: Name of the signal source
    """
    logging.info("the %s must be ON", signal_source_name)
    if signal_source_name.startswith("Band"):
        current_value = getattr(skysim_controller_device, "Band")
        expected_band = BandState[signal_source_name.upper()]
        assert current_value == expected_band
    else:
        current_value = getattr(skysim_controller_device, signal_source_name)
        assert current_value
