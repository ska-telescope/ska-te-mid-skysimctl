{{/*
Expand the name of the chart.
*/}}
{{- define "alarmHandler.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "alarmHandler.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "alarmHandler.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "alarmHandler.labels" -}}
helm.sh/chart: {{ include "alarmHandler.chart" . }}
{{ include "alarmHandler.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}
{{/*
Test-labels
*/}}
{{- define "alarmHandler.test-labels" -}}
helm.sh/chart: {{ include "alarmHandler.chart" . }}
app.kubernetes.io/name: {{ include "alarmHandler.name" . }}-test-connection
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "alarmHandler.selectorLabels" -}}
app.kubernetes.io/name: {{ include "alarmHandler.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "alarmHandler.imageName" -}}
{{- if .Values.alarmHandler.image.digest -}}
{{- printf " \"%s/%s@%s\"" .Values.alarmHandler.image.registry .Values.alarmHandler.image.name .Values.alarmHandler.image.digest }}
{{- else -}}
{{- printf " \"%s/%s:%s\"" .Values.alarmHandler.image.registry .Values.alarmHandler.image.name .Values.alarmHandler.image.version }}
{{- end -}}
{{- end }}
