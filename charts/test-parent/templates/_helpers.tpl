{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "slackWebserver.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "slackWebserver.labels" -}}
helm.sh/chart: {{ include "slackWebserver.chart" . }}
{{ include "slackWebserver.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}
{{/*
Test-labels
*/}}
{{- define "slackWebserver.test-labels" -}}
helm.sh/chart: {{ include "slackWebserver.chart" . }}
app.kubernetes.io/name: {{ .Values.slackWebserver.name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "slackWebserver.selectorLabels" -}}
app.kubernetes.io/name: {{ .Values.slackWebserver.name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "slackWebserver.imageName" -}}
{{- printf " \"%s/%s:%s\"" .Values.image.registry .Values.slackWebserver.imageName .Values.image.tag }}
{{- end }}

{{- define "slackWebserver.serviceType" }}
{{- if eq .Values.env.type "dev" -}}
NodePort
{{- else -}}
LoadBalancer
{{- end }}
{{- end }}

{{- define "pullPolicy" }}
{{- if .Values.imageoverride -}}
IfNotPresent
{{- else if eq .Values.env.type "production" -}}
{{ .Values.imagePullPolicy }}
{{- else if eq .Values.env.type "ci" -}}
Always
{{- else -}}
Never
{{- end }}
{{- end }}

{{- define "var_dump" -}}
{{- . | mustToPrettyJson | printf "\nThe JSON output of the dumped var is: \n%s" | fail }}
{{- end -}}