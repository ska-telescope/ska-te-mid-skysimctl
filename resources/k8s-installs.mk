
# Set to false in gitlab jobs; true for local testing
MINIKUBE ?= true

# Overriden in gitlab production deployment job
ALARM_APP := skysimctl-alarm-handler
ALARM_RELEASE := skysimctl-alarm-handler
ALARM_CHART := ska-te-mid-skysimctl-alarm-handler

itf-alarm-install: K8S_CHART_PARAMS := $(K8S_CHART_PARAMS) --set alarmHandler.skysim_alarm_webhook=$(SKYSIM_ALARM_WEBHOOK) \
	--set global.minikube=$(MINIKUBE) --set global.cluster_domain=$(CLUSTER_DOMAIN)
itf-alarm-install: KUBE_APP := $(ALARM_APP)
itf-alarm-install: HELM_RELEASE := $(ALARM_RELEASE)
itf-alarm-install: K8S_CHART := $(ALARM_CHART)
itf-alarm-install: k8s-template-chart k8s-install-chart k8s-wait
.PHONY: itf-alarm-install

itf-alarm-uninstall: KUBE_APP := $(ALARM_APP)
itf-alarm-uninstall: HELM_RELEASE := $(ALARM_RELEASE)
itf-alarm-uninstall: K8S_CHART := $(ALARM_CHART)
itf-alarm-uninstall: k8s-uninstall-chart
.PHONY: itf-alarm-uninstall
