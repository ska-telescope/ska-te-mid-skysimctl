skysimctl-vars:
	@echo "################################################################"; \
	$(info PYTHON_VARS_BEFORE_PYTEST: $(PYTHON_VARS_BEFORE_PYTEST))
	$(info K8S_CHART: $(K8S_CHART))
	$(info K8S_CHARTS: $(K8S_CHARTS))
	$(info HELM_CHARTS: $(HELM_CHARTS))
	$(info SKA_TANGO_OPERATOR: $(SKA_TANGO_OPERATOR))
	$(info JIRA_AUTH: $(JIRA_AUTH))
	$(info CI_RUNNER_TAGS: $(CI_RUNNER_TAGS))
