.DEFAULT_GOAL := help

# define overides for above variables in here
-include PrivateRules.mak

PLAYBOOK_PARAMETERS ?= ## any additional ansible tags can go here - see ansible-playbook --help
ANSIBLE_USER ?= $(USER)
V ?=

MKFILE_PATH := $(abspath $(firstword $(MAKEFILE_LIST)))
MKFILE_DIR := $(dir $(MKFILE_PATH))
PYTHON_PATH ?= /usr/bin/python3

# PI_VERSION should be pi4
PI_VERSION ?= pi4
# You can set this to pi4-remote when testing locally
PI_HOST ?= $(PI_VERSION)
INVENTORY_FILE ?= $(MKFILE_DIR)/inventory/hosts
PI_VARS_FILE ?= ./group_vars/$(PI_VERSION)/$(PI_VERSION).yaml
ANSIBLE_COMMAND := poetry run ansible-playbook -i $(INVENTORY_FILE) -e @$(PI_VARS_FILE) --limit ${PI_HOST} $(PLAYBOOK_PARAMETERS) -u=$(ANSIBLE_USER) --extra-vars "tango_host=$(TANGO_HOST)" $(V)
ANSIBLE_COMMAND_DRY_RUN := $(ANSIBLE_COMMAND) --check --diff

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "PLAYBOOK_PARAMETERS=$(PLAYBOOK_PARAMETERS)"
	@echo "ANSIBLE_USER=$(ANSIBLE_USER)"
	@echo "ANSIBLE_COMMAND=$(ANSIBLE_COMMAND)"
	@echo "ANSIBLE_COMMAND_DRY_RUN=$(ANSIBLE_COMMAND_DRY_RUN)"
	@echo "V=$(V)"
	@echo "MKFILE_PATH=$(MKFILE_PATH)"
	@echo "MKFILE_DIR=$(MKFILE_DIR)"
.PHONY: vars

SHELL := /bin/bash

setup_env: ## Setup venv and export requirements.txt.
	cd $(MKFILE_DIR) && \
	sudo apt update && \
	sudo apt install -y pipx && \
	pipx install poetry && \
	pipx ensurepath && \
	poetry env use $(PYTHON_PATH) && \
	poetry lock && \
	poetry self add poetry-plugin-export && \
	poetry install
.PHONY: setup_env

webserver_setup: setup_env ## Export requirements.txt from poetry for webserver.
	mkdir -p $(MKFILE_DIR)/roles/webserver/files && \
	cd $(MKFILE_DIR)/../../ && \
	poetry export --only webserver --without-hashes > $(MKFILE_DIR)/roles/webserver/files/requirements.txt
.PHONY: webserver_setup

skysimctl_pi0_setup: setup_env
.PHONY: skysimctl_pi0_setup

skysimctl_pi4_setup: setup_env
	mkdir -p $(MKFILE_DIR)/roles/skysimctl/files && \
	cd $(MKFILE_DIR)/../../ && \
	poetry export --only skysimctl --without-hashes > $(MKFILE_DIR)/roles/skysimctl/files/requirements-pi4.txt
.PHONY: skysimctl_pi0_setup

deploy_webserver: webserver_setup ## Runs the deploy playbook on the Raspberry Pi.
	$(ANSIBLE_COMMAND) $(MKFILE_DIR)/webserver.yaml
.PHONY: deploy_webserver

deploy_webserver_dry_run: webserver_setup ## Runs the deploy playbook on the Raspberry Pi in dry-run mode.
	$(ANSIBLE_COMMAND_DRY_RUN) $(MKFILE_DIR)/webserver.yaml
.PHONY: deploy_webserver_dry_run

deploy_skysimctl: skysimctl_$(PI_VERSION)_setup ## Runs the deploy playbook on the Raspberry Pi.
	$(ANSIBLE_COMMAND) $(MKFILE_DIR)/skysimctl.yaml
.PHONY: deploy_skysimctl

deploy_skysimctl_dry_run: skysimctl_$(PI_VERSION)_setup ## Runs the deploy playbook on the Raspberry Pi in dry-run mode.
	$(ANSIBLE_COMMAND_DRY_RUN) $(MKFILE_DIR)/skysimctl.yaml
.PHONY: deploy_skysimctl


lint: ## Lint check playbook
	ansible-lint -vvv $(MKFILE_DIR)/site.yml
.PHONY: lint

list_tasks: ## Prints the tasks to be executed during a playbook run.
	ansible-playbook -i $(INVENTORY_FILE) \
		skysimctl.yml \
		$(PLAYBOOK_PARAMETERS) \
		--list-tasks
	ansible-playbook -i $(INVENTORY_FILE) \
		webserver.yml \
		$(PLAYBOOK_PARAMETERS) \
		--list-tasks
.PHONY: list_tasks

# Note: in order for a target to show up in the help message,
# it needs to have a comment starting with '##' on the same line.
help:  ## Show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help

echo_vars:
	@echo "Variables:"
	@echo $(PRIVATE_VARS)
.PHONY: echo_vars
