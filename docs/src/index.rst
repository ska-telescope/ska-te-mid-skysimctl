.. deploy_itf_mid documentation master file, created by
   ska-ser-sphinx-templates bootstrap
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the SKA Mid Sky Simulator Controller documentation!
==============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Readme

   README

.. toctree::
   :maxdepth: 2
   :caption: Introduction

   purpose

Indices and tables
==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
